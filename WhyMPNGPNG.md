Why use MPNGPNG instead of APNG/MNG?
====================================

1. Composing a single image out of multiple images
--------------------------------------------------

MPNGPNG lets you draw e.g. 4 images in a single frame, to make a frame with the 4 images side-by-side. (this isn't very useful on its own, but see below)

2. Lazy multi-animation syncing
-------------------------------

Say you want to put 4 animations running in parallel on the same image. With GIF or APNG, you'd have to find the "sync point" where all 4 animations sync up, e.g.:

```
Animation 1: |  |  |  |  |  |  |  |  |
Animation 2: |   |   |   |   |   |   |
Animation 3: |     |     |     |     |
Animation 4: |       |       |       |
```

With MPNGPNG, you can include the 4 animations as 4 separate animations and tell it to compose an image with the 4 animations running in parallel, so that you only have:

```
Animation 1: |  |
Animation 2: |   |
Animation 3: |     |
Animation 4: |       |
```

It's pretty obvious that MPNGPNG would be using less space!